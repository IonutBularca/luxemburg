
// ChildView.cpp : implementation of the CChildView class
//

#include "stdafx.h"
#include "MFCApplication2.h"
#include "ChildView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#include <vector>
#include <queue>
#include <fstream>

#include "globals.h"

// CChildView

CChildView::CChildView()
{
	
}

CChildView::~CChildView()
{
}


BEGIN_MESSAGE_MAP(CChildView, CWnd)
	ON_WM_PAINT()
//	ON_WM_LBUTTONDOWN()
ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()



// CChildView message handlers

BOOL CChildView::PreCreateWindow(CREATESTRUCT& cs) 
{
	if (!CWnd::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_CLIENTEDGE;
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW|CS_VREDRAW|CS_DBLCLKS, 
		::LoadCursor(nullptr, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW+1), nullptr);

	return TRUE;
}


#include <fstream>
#include <vector>
#include <tuple>



//void DoSomethingOnAnotherThread(CEvent* event, CPaintDC* dc)
//{
//
//	while(GetKeyState(WM_LBUTTONDOWN) > 0) {
//		dc->Ellipse(122, 600, 5, 5);
//	}
//
//		event->SetEvent();
//
//}


void CChildView::OnPaint() 
{

//	std::vector<Node> coordinatesForDrawing;
//	std::vector<Arch> arches;
	CPaintDC dc(this); // device context for painting
	//---------------------------------
	
	CPen normalPen(PS_SOLID, 1, RGB(21, 131, 255));
	dc.SelectObject(normalPen);
	for (auto&& a : arches) {
		dc.MoveTo(coordinatesForDrawing[std::get<0>(a)].pos.first, coordinatesForDrawing[std::get<0>(a)].pos.second);
		dc.LineTo(coordinatesForDrawing[std::get<1>(a)].pos.first, coordinatesForDrawing[std::get<1>(a)].pos.second);
	}
	if (done) {
		CPen normalPen(PS_SOLID, 2, RGB(255, 0, 0));
		dc.SelectObject(normalPen);
	//	dc.MoveTo(coordinatesForDrawing[start].pos.first,coordinatesForDrawing[start].pos.second);
	//	dc.LineTo(coordinatesForDrawing[stop].pos.first, coordinatesForDrawing[stop].pos.second);
		for (int i = 0; i < rezultat.size(); i++) {
			dc.MoveTo(coordinatesForDrawing[rezultat[i].first].pos.first, coordinatesForDrawing[rezultat[i].first].pos.second);
			dc.LineTo(coordinatesForDrawing[rezultat[i].second].pos.first, coordinatesForDrawing[rezultat[i].second].pos.second);
		}
	}
}





