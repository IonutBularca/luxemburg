#pragma once

#include <vector>
#include <tuple>

using Coord = std::pair<int, int>;

struct Node {
	Coord pos;
	std::vector<std::pair<int, int>> vecini;
};

using Arch = std::tuple<int, int, int>;



extern std::vector<Node> coordinatesForDrawing;
extern std::vector<Arch> arches;

extern int firstX, firstY, secondX, secondY;

extern bool done;

extern int start, stop;

extern 	std::vector<std::pair<int, int>> rezultat;