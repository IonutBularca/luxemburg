
// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "MFCApplication2.h"

#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#include "globals.h"
std::vector<Node> coordinatesForDrawing;
std::vector<Arch> arches;
int step = 0;
int firstX, firstY, secondX, secondY;

std::vector<std::pair<int, int>> rezultat;

bool done;

int start, stop;

std::vector<Node> N;
// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
	ON_WM_SETFOCUS()
	ON_WM_MOUSEACTIVATE()
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

// CMainFrame construction/destruction

CMainFrame::CMainFrame() noexcept
{
	// TODO: add member initialization code here
}

CMainFrame::~CMainFrame()
{
}
#include <fstream>
int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{

	done = false;

	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// create a view to occupy the client area of the frame
	if (!m_wndView.Create(nullptr, nullptr, AFX_WS_DEFAULT_VIEW, CRect(0, 0, 0, 0), this, AFX_IDW_PANE_FIRST, nullptr))
	{
		TRACE0("Failed to create view window\n");
		return -1;
	}

	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP | CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	if (!m_wndStatusBar.Create(this))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}
	m_wndStatusBar.SetIndicators(indicators, sizeof(indicators)/sizeof(UINT));

	// TODO: Delete these three lines if you don't want the toolbar to be dockable
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);




	int minLong = 0, maxLong, minLat, maxLat;

	std::ifstream in("nodes.csv");
	int nNodes;
	in >> nNodes;

	//	std::cout << nNodes;
		//system("pause");
	for (int i = 0; i < nNodes; i++) {
		Node temp;
		in >> temp.pos.first >> temp.pos.second;
		if (minLong == 0) {
			minLat = temp.pos.first;
			maxLat = temp.pos.first;
			minLong = temp.pos.second;
			maxLong = temp.pos.second;
		}
		else {
			if (temp.pos.first < minLat) {
				minLat = temp.pos.first;
			}
			if (temp.pos.first > maxLat) {
				maxLat = temp.pos.first;
			}
			if (temp.pos.second < minLong) {
				minLong = temp.pos.second;
			}
			if (temp.pos.second > maxLong) {
				maxLong = temp.pos.second;
			}
		}
		N.push_back(temp);
	}
	maxLong = maxLong - minLong;
	maxLat = maxLat - minLat;
	//aici s-ar putea sa fie latitudinea si longitudinea invers
	const double xPercent = 1300.0 / maxLong;
	const double yPercent = 600.0 / maxLat;
	//	std::cout << xPercent << ' ' << yPercent << std::endl;
	for (auto&& a : N) {
		a.pos.first = a.pos.first - minLat;
		a.pos.second = a.pos.second - minLong;
		Node temp;
		temp.pos.first = a.pos.first * xPercent;
		temp.pos.second = a.pos.second * yPercent;
		coordinatesForDrawing.push_back(temp);
	}
	minLat = 0;
	minLong = 0;
	/*	for (auto&& a : N) {
			std::cout << a.pos.first << " " << a.pos.second << std::endl;
		}
		*/
		/*for (auto&& a : coordinatesForDrawing) {
			std::cout << a.pos.first << " " << a.pos.second << std::endl;
		}*/
	int nArches;
	in >> nArches;

	for (int i = 0; i < nArches; i++) {
		Arch temp;
		in >> std::get<0>(temp) >> std::get<1>(temp) >> std::get<2>(temp);
		arches.push_back(temp);
		N[std::get<0>(temp)].vecini.push_back(std::make_pair(std::get<1>(temp), std::get<2>(temp)));
		N[std::get<1>(temp)].vecini.push_back(std::make_pair(std::get<0>(temp),std::get<2>(temp)));
	}

	//	std::cout <<std::endl<< maxLat << " " << maxLong << std::endl;

		//system("pause");



	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.lpszClass = AfxRegisterWndClass(0);
	cs.cy = 800;
	cs.cx = 1600;
	cs.x = 0;
	cs.y =0;
	return TRUE;
}

// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}
#endif //_DEBUG


// CMainFrame message handlers

void CMainFrame::OnSetFocus(CWnd* /*pOldWnd*/)
{
	// forward focus to the view window
	m_wndView.SetFocus();
}

BOOL CMainFrame::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo)
{
	// let the view have first crack at the command
	if (m_wndView.OnCmdMsg(nID, nCode, pExtra, pHandlerInfo))
		return TRUE;

	// otherwise, do default handling
	return CFrameWnd::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}

#include <algorithm>
#include <queue>
std::vector<int> d;
class comp {
		
public:
	bool operator ()(int& a, int& b) {
		return d[a] > d[b];
	}

};

int CMainFrame::OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message)
{
	// TODO: Add your message handler code here and/or call default
	if (step%3 == 0) {
		//
		rezultat.clear();
		//
		step++;
		POINT p;
		GetCursorPos(&p);
		firstX = p.x-10;
		
		firstY = p.y-78;
		
		return CFrameWnd::OnMouseActivate(pDesktopWnd, nHitTest, message);
	}
	if (step%3 == 1) {
		step++;
		POINT p;
		GetCursorPos(&p);
		secondX = p.x-10;
		
		secondY = p.y-78;
	
	}

	if (step%3 == 2){

		//-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=
		start = 0;
		stop = 0;
		float min1, min2;
		min1 = sqrt((coordinatesForDrawing[start].pos.first-firstX)*(coordinatesForDrawing[start].pos.first - firstX) + (coordinatesForDrawing[start].pos.second - firstY )*(coordinatesForDrawing[start].pos.second - firstY) );
		min2 = sqrt((coordinatesForDrawing[stop].pos.first - secondX)*(coordinatesForDrawing[stop].pos.first - secondX) + (coordinatesForDrawing[stop].pos.second - secondY)*(coordinatesForDrawing[stop].pos.second - secondY));

		for (int i = 0; i < coordinatesForDrawing.size(); i++) {
			if (min1 > sqrt((coordinatesForDrawing[i].pos.first - firstX)*(coordinatesForDrawing[i].pos.first - firstX) + (coordinatesForDrawing[i].pos.second - firstY)*(coordinatesForDrawing[i].pos.second - firstY))) {
				min1 = sqrt((coordinatesForDrawing[i].pos.first - firstX)*(coordinatesForDrawing[i].pos.first - firstX) + (coordinatesForDrawing[i].pos.second - firstY)*(coordinatesForDrawing[i].pos.second - firstY));
				start = i;
			}
			if (min2 > sqrt((coordinatesForDrawing[i].pos.first - secondX)*(coordinatesForDrawing[i].pos.first - secondX) + (coordinatesForDrawing[i].pos.second - secondY)*(coordinatesForDrawing[i].pos.second - secondY))) {
				min2 = sqrt((coordinatesForDrawing[i].pos.first - secondX)*(coordinatesForDrawing[i].pos.first - secondX) + (coordinatesForDrawing[i].pos.second - secondY)*(coordinatesForDrawing[i].pos.second - secondY));
				stop = i;
			}
		}
		//-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=
		
		//-------------------------------------------------
		//-------implementarea  algoritmului din curs------

		std::vector<int> W;
		W.resize(N.size());
	
		d.resize(N.size());
		std::vector<int> p;
		p.resize(N.size());
		for (int i = 0; i < N.size(); i++) {
			W[i] = 0;
			d[i] = MAXINT;
			p[i] = 0;
		}
		d[start] = 0;
		p[start] = 0;
		int Wsize = W.size();
		
		std::priority_queue<int,std::vector<int>, comp > coada;
		coada.push(start);
		while (!coada.empty()) {
			
			int x=0 ;
			bool done = false;
			
			//se selecteaza x din W cu d[x] minim
	/*		for (int i = 0; i < d.size(); i++) {
				if (!done && W[i] != -1) {
					x = i;
					done = true;
				}
				if (done && d[x] > d[i] && W[i]!=-1) {
					x = i;
				}
			}
			*/
			x = coada.top();
			if (x == stop)
				break;
			coada.pop();
			//----------------------
			//scoatem x din w
			//----------------------
			W[x] = -1;
			Wsize = Wsize - 1;
			//----------------------
			//se cauta acel nod y  (FOR)
			
			for (int i = 0; i < N[x].vecini.size(); i++) {
				if (W[N[x].vecini[i].first] != -1) {
					if (d[x] + N[x].vecini[i].second < d[N[x].vecini[i].first]) {
						d[N[x].vecini[i].first] = d[x] + N[x].vecini[i].second;
						p[N[x].vecini[i].first] = x;
						coada.push(N[x].vecini[i].first);
					}
				}
			}
			
			
		}
			
		//-------------------------------------------------
		
	

		int current = stop;
		while (p[current] != 0) {
			rezultat.push_back(std::make_pair(current, p[current]));
			current = p[current];
		}


		//------------------------------------------------
		Invalidate();
		OnPaint();
		step++;
		done = true;
		return CFrameWnd::OnMouseActivate(pDesktopWnd, nHitTest, message);
	}
	return CFrameWnd::OnMouseActivate(pDesktopWnd, nHitTest, message);
}
